package tests;

import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.BeforeAll;
import utils.DriverSetup;

import java.net.MalformedURLException;

// От данной страницы обычно наследуются все остальные тестовые классы
// Обычно здесь описаны действия которые нужно делать везде, чтобы не дублировать код
// Например в текущем случаем поднятие драйвера и подвязка скриншотов к отчеты
// Как пример также в такие классы могут быть вынесены методы логина/логаута

public class BaseTest {

    @BeforeAll
     static void setUp() {
        DriverSetup.setupDriver();
        // Привязка скриншотов которые делает селенид к allure report
       // SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));
    }
}
