package tests;


import com.codeborne.selenide.Selenide;
import data_providers.CredentialsProvider;
import data_models.LoginModel;
import example_pages.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import utils.DriverSetup;

import static org.junit.jupiter.api.parallel.ExecutionMode.SAME_THREAD;

@Execution(SAME_THREAD) // Для параллельного выполнения тестов вида 1 поток 1 класс
@Tag("Execute")
public class ExampleSelenideTests extends BaseTest {

    @Test
    void handleBasicAuth() {
        Menu menu = new Menu();
        BasicAuth basicAuth = menu.passBaseAuthentication();
        basicAuth.checkAuthIsPassed();
    }

    @Test
    void workingWithDropdown() {
        Menu menu = new Menu();
        Dropdown dropdown = menu.openDropdownPage();
        dropdown.selectValue("Option 2");
        dropdown.checkSelectedValue("Option 2");
    }

    @Test
    public void brokenImages() {
        Menu menu = new Menu();
        BrokenImages brokenImages = menu.openBrokenImages();
        brokenImages.checkThereAreNoBrokenImages();
    }

    @Test
    public void checkboxes() {
        Menu menu = new Menu(); // Открываем главную страницу и получаем доступ к методам класса Menu
        Checkboxes checkboxes = menu.openCheckboxes(); // С помощью описаного метода открываем страницу Checkboxes
                            // здесь же получаем обьект этого класса чтобы иметь возможность работать со страницей checkboxes
        checkboxes.selectCheckbox(); // Выполняем какое-то действия, в даном случае выбор чекбокса
        checkboxes.checkboxShouldBeSelected(); // Тест всегда оканчивается проверкой, в даном случае проверкой что чекбокс был выбран
    }

    @Disabled
    @Test
    public void dragAndDrop() {
        Menu menu = new Menu();
        DragAndDrop dragAndDrop = menu.openDragAndDrop();
        dragAndDrop.dragColumnAToColumnB();
        dragAndDrop.checkDragAndDropIsSuccessful();
    }

    @Test
    public void uploadFile() {
        Menu menu = new Menu();
        FileUpload fileUpload = menu.openFileUpload();
        fileUpload.uploadFiles("room1.jpg").submit();
        fileUpload.checkFileIsUploaded("room1.jpg");
    }

    // Пример параметризированого теста
    // Иногда нужно прогнать один и тот же тест на разных данных, это пример возможной реализации
    // Варианты передавать данные самые разные
    // Здесь можно ознакомиться подробнее https://www.petrikainulainen.net/programming/testing/junit-5-tutorial-writing-parameterized-tests/
    // В данном примере данные приходят из кастомных DataProvider классов, расположены в src/test/java/DataProviders
    @DisplayName("Login tests")
    @ParameterizedTest(name = "run #{index} with [{arguments}]")
    @ArgumentsSource(CredentialsProvider.class)
    public void formAuthentication(LoginModel credentials, String expectedMessage) {
        Menu menu = new Menu();
        FormAuthentication formAuthentication = menu.openFormAuthentication();
        formAuthentication.fillLoginForm(credentials).submitLogin();
        formAuthentication.checkLoginResult(expectedMessage);
    }

    @Test
    public void sortTable() {
        Menu menu = new Menu();
        SortableDataTables dataTables = menu.openSortableDataTables();
        dataTables.ascSortByLastName().checkSortResult();
    }

    @Test
    public void sortTableWithoutClasses() {
        Menu menu = new Menu();
        SortableDataTables dataTables = menu.openSortableDataTables();
        dataTables.ascSortByLastNameWithClass().checkSortResultForTableWithoutClasses();
    }

    @Test
    public void softAssertExample() {
        Menu menu = new Menu();
        SortableDataTables dataTables = menu.openSortableDataTables();
        dataTables.checkFirstRowData();
    }



}
