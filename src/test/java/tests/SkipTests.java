package tests;

import example_pages.Checkboxes;
import example_pages.Dropdown;
import example_pages.Menu;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;

import static org.junit.jupiter.api.parallel.ExecutionMode.SAME_THREAD;

// Класс для gradle файла чтобы показать работу @Tag

@Execution(SAME_THREAD)
@Tag("ForSkip")
public class SkipTests extends BaseTest {

    @Test
    public void betterSkipThis() {
        Menu menu = new Menu();
        Dropdown dropdown = menu.openDropdownPage();
        dropdown.selectValue("Option 2");
        dropdown.checkSelectedValue("Option 2");
    }

    @Test
    public void checkboxes() {
        Menu menu = new Menu();
        Checkboxes checkboxes = menu.openCheckboxes();
        checkboxes.selectCheckbox();
        checkboxes.checkboxShouldBeSelected();
    }

}
