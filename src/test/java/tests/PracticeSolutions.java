package tests;

import com.codeborne.selenide.*;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.util.List;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;


public class PracticeSolutions {


    @Test
    void lesson1() {
        // настройки для запуска браузера
        // Configuration.browser ="chrome"; // или "firefox"
        Configuration.browserSize = "1600x900";
        Configuration.timeout = 10000;
        Configuration.proxyEnabled = true;
        Configuration.proxyHost = "my-proxy-host";
        Configuration.proxyPort = 1234;
        open("https://www.google.com");
    }


    @Test
    void lesson5_1() {
        // Открываем страницу авторизации
        open("https://onix-systems-opencart.staging.onix.ua/");
   /*   $x("//*[text()='My Account']").click();
        $x("//*[text()='Register']").click();
        $(By.id("input-firstname")).setValue("testFirst");
        $(By.id("input-lastname")).setValue("testLast");
        $(By.id("input-email")).setValue("lingrainetest@gmail.com");
        $(By.id("input-password")).setValue("testPass");
        $(By.name("agree")).click();
        $x("//*[@type='submit']").click();
        $(By.tagName("h1")).shouldHave(text("Your Account Has Been Created"));*/



        $x("//*[text()='My Account']").click();
        $x("//*[text()='Login']").click();
        $(By.id("input-email")).setValue("lingrainetest@gmail.com");
        $(By.id("input-password")).setValue("testPass");
        $x("//*[@type='submit']").click();
        $x("//aside/descendant::a[text()='Logout']").shouldBe(visible);

    }


    @Test
    void lesson5_2() {

        // Открыть браузер и перейти на страницу Google
        open("https://www.google.com/");

        // Ввести в поле поиска слово "Selenide" и нажать клавишу Enter
        $(By.name("q")).setValue("Selenide").pressEnter();

        // Проверить, что заголовок страницы содержит слово "Selenide"
        ElementsCollection titles = $$("title");
        String titleText = Selenide.title();
        Assertions.assertTrue(titleText.contains("Selenide"));

        // Получить коллекцию всех ссылок на странице и проверить, что количество ссылок больше 5
        ElementsCollection links = $$(By.cssSelector("#search a"));
        links.shouldHave(sizeGreaterThan(5));

        // Получить коллекцию всех заголовков и проверить, что количество заголовков больше 5
        ElementsCollection headings = $$(By.cssSelector("#search h3"));
        List<String> headers = $$(By.cssSelector("#search h3")).texts();
        headings.shouldHave(sizeGreaterThan(5));

        // Найти и кликнуть на первую ссылку из коллекции
        headings.get(0).click();

        // Проверить, что заголовок страницы содержит текст из кликнутой ссылки
        Assertions.assertEquals(headers.get(0),Selenide.title());
    }

    @Test
    void lesson6_1() throws FileNotFoundException {
        open("http://the-internet.herokuapp.com/download");
        File download = $(By.linkText("sample.png")).download();
        //Assertions.assertTrue(FileUtils.contentEquals(etalonFile, download)); // EtalonFile - заранее приготовленный файл если есть необходимость проверять что скачивается какой-то конкретный файл
    }

    @Test
    void lesson6_2() {
        open("http://the-internet.herokuapp.com/iframe");
        Selenide.switchTo().frame("mce_0_ifr");
        $("#tinymce").setValue("some text");
        System.out.println("stop");

    }

    @Test
    void lesson6_3() {
        open("http://the-internet.herokuapp.com/windows");
        $x("//a[text()='Click Here']").click();
        String originalWindow = getWebDriver().getWindowHandle();
        //Selenide.switchTo().window(1);
        //Selenide.switchTo().window("New Window");
        $("h3").shouldHave(text("New Window"));
        //Selenide.switchTo().window(0);
        //Selenide.switchTo().window("The Internet");
        $("h3").shouldHave(text("Opening a new window"));
        System.out.println("stop");

    }

    @Test
    void lesson6_4() {
        open("http://the-internet.herokuapp.com/javascript_alerts");
        $x("//button[text()='Click for JS Alert']").click();
        confirm();
        $x("//button[text()='Click for JS Confirm']").click();
        dismiss();
        $x("//button[text()='Click for JS Prompt']").click();
        prompt("I am a JS prompt", "qw");

    }

    @Test
    public void testWindowsTabsAlerts() {
        // открываем сайт
        open("https://the-internet.herokuapp.com/");

        // открываем страницу Multiple Windows и переключаемся на новое окно
        $("a[href='/windows']").click();
        $("a[href='/windows/new']").click();
        switchTo().window(1);
        Assertions.assertTrue(title().contains("New Window"));

        // закрываем новое окно и переключаемся на первое окно
        closeWindow();
        switchTo().window(0);
        back();

        // открываем страницу Nested Frames и переключаемся на фрейм Frame Top
        $("a[href='/nested_frames']").click();
        switchTo().frame("frame-top");

        // переключаемся на фрейм Frame Left и проверяем текст на странице
        switchTo().frame("frame-left");
        Assertions.assertTrue($("body").text().equals("LEFT"));

        // переключаемся на фрейм Frame Middle и проверяем текст на странице
        switchTo().defaultContent();
        switchTo().frame("frame-top");
        switchTo().frame("frame-middle");
        Assertions.assertTrue($("body").text().equals("MIDDLE"));

        // переключаемся на фрейм Frame Right и проверяем текст на странице
        switchTo().defaultContent();
        switchTo().frame("frame-top");
        switchTo().frame("frame-right");
        Assertions.assertTrue($("body").text().equals("RIGHT"));

        // переключаемся на фрейм Frame Bottom и проверяем текст на странице
        switchTo().defaultContent();
        switchTo().frame("frame-bottom");
        Assertions.assertTrue($("body").text().equals("BOTTOM"));

        // возвращаемся на главную страницу и открываем страницу JavaScript Alerts
        switchTo().defaultContent();
        back();
        $("a[href='/javascript_alerts']").click();

        // проверяем алерт "JS Alert" и нажимаем на кнопку "OK"
        $("button[onclick='jsAlert()']").click();
        switchTo().alert().accept();
        Assertions.assertTrue($(By.id("result")).text().equals("You successfully clicked an alert"));

        // проверяем алерт "JS Confirm" и нажимаем на кнопку "Cancel"
        $("button[onclick='jsConfirm()']").click();
        switchTo().alert().dismiss();
        Assertions.assertTrue($(By.id("result")).text().equals("You clicked: Cancel"));

        // проверяем алерт "JS Prompt", вводим текст и нажимаем на кнопку "OK"
        $("button[onclick='jsPrompt()']").click();
        switchTo().alert().sendKeys("Test");
        switchTo().alert().accept();
        Assertions.assertTrue($(By.id("result")).text().equals("You entered: Test"));
    }

    @Test
    void waitsExample() {
        open("https://onix-systems-opencart.staging.onix.ua/");
        $x("//*[@alt='iPhone 6']").shouldBe(visible);
        $x("//*[@alt='iPhone 6']").should(disappear, Duration.ofSeconds(8));
        $x("//*[@alt='MacBookAir']").shouldBe(visible);
        $x("//*[@alt='MacBookAir']").should(disappear, Duration.ofSeconds(8));
        $x("//*[@alt='iPhone 6']").shouldBe(visible);
    }
}
