package utils;
import com.codeborne.selenide.Configuration;






public class DriverSetup {
    public static String baseUrl = "http://the-internet.herokuapp.com/"; // Application under test

    // Инициализация и настройки браузер драйвера
    public static void setupDriver()  {
        Configuration.browser = "chrome"; // change for utils.SelenoidWebDriverProvider to use Selenoid
        Configuration.timeout = 7000; // Стандартная задержка для selenide методов
        Configuration.fastSetValue = false; // способ работы sendKeys при false эмулируется пользовательская клавиатура,
                                    // при true sendKeys с помощью js напрямую меняет value у input
        Configuration.holdBrowserOpen = true;   // debug mode, opened browser after test is finished

        // Условие для запуска в дженкинсе если у вас много тестовых инстансов и урл передаётся из вне
        // Тогда в случае локального запуска дефолтный урл будет http://localhost:8080 и нужно будет переназначить base url
        // В случае если тестовый инстанс один то данную конструкцию можно убирать и оставить просто
        // Configuration.baseUrl = baseUrl;

        if(Configuration.baseUrl.equals("http://localhost:8080")) {
            Configuration.baseUrl = baseUrl;
        }

    }


}
