package data;


// Это один из вариантов хранения тестовых данных
// Этот момент нужно расматривать в зависимости от проекта
// Основные возможные альтернативные варианты хранения данных ( файлы (текстовые, yml, json), хранение данных в ввиде sql запросов)
// Стоит избегать захардкоженных данных в теле теста/метода, так с вероятностью 90 процентов возникнет потребность проверять кейс на другом наборе данных
// Следует изначально писать методы чтобы данные можно было заменить легко и просто

import data_models.LoginModel;

public class Credentials {
    public static final LoginModel validUser = new LoginModel("tomsmith", "SuperSecretPassword!");
    public static LoginModel userWithInvalidCredentials = new LoginModel("invalidUser ", "12345678");

}
