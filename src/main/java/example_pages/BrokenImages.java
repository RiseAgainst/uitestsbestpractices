package example_pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$;


public class BrokenImages {
    private ElementsCollection images = $$(By.tagName("img"));
    List<String> brokenImages = new ArrayList<>();


    // Находим все картинки на странице
    // Проверяем отображаются ли они с помощью параметра naturalWidth, если он 0 значит картинка не отображается
    // Собираем поломанные картинки в массив и выводим в репорте


    // Есть альтернативные способы, например подключать прокси -> фильтровать запросы чтобы отобрать те которые дёргают картинки и проверять их код ответа

    public void checkThereAreNoBrokenImages() {
       for(SelenideElement image: images) {
           isImageBroken(image);
       }
       Assertions.assertTrue(brokenImages.isEmpty(), printResult());
    }

    public void isImageBroken(SelenideElement image) {
        if (image.getAttribute("naturalWidth").equals("0"))
        {
            brokenImages.add(image.getAttribute("outerHTML") + " is broken.");
        }
    }

    public String printResult() {
        String message = "\n";
        for(String item : brokenImages) {
            message += item + "\n";
        }
        return message;
    }
}
