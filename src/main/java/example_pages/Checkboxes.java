package example_pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class Checkboxes {
    private SelenideElement checkbox1 = $(By.id("checkboxes")).find(By.tagName("input"));
    private SelenideElement ch = $(By.cssSelector(".my-element"));



    public void checkboxShouldBeSelected() {
        checkbox1.shouldNotBe(Condition.visible);
    }

    public void selectCheckbox() {
        checkbox1.click();
    }
    public void checkText() {
        ch.shouldHave(Condition.text("saas"));
    }

}
