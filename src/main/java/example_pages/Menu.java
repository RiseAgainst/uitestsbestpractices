package example_pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class Menu {
    private static SelenideElement basicAuth = $(By.xpath("//*[contains(text(), 'Basic Auth')]"));
    private static SelenideElement brokenImages = $(By.xpath("//*[contains(text(), 'Broken Images')]"));
    private static SelenideElement fileUpload = $(By.xpath("//*[contains(text(), 'File Upload')]"));
    private static SelenideElement dropdown = $(By.xpath("//*[contains(text(), 'Dropdown')]"));
    private static SelenideElement checkboxes = $(By.xpath("//*[contains(text(), 'Checkboxes')]"));
    private static SelenideElement dragAndDrop = $(By.xpath("//*[contains(text(), 'Drag and Drop')]"));
    private static SelenideElement formAuthentication = $(By.xpath("//*[contains(text(), 'Form Authentication')]"));
    private static SelenideElement sortableDataTables = $(By.xpath("//*[contains(text(), 'Sortable Data Tables')]"));

    public Menu() {
        open("");
    }

    // использовать метод open в таком виде это тоже самое что открыть в браузере вот такой урл
    // https://admin:admin@http://the-internet.herokuapp.com/basic_auth
    // используя особености http протокола мы передаём кредентиалс в стандартном хедере Authorization протокола http
    // https://serverfault.com/questions/371907/can-you-pass-user-pass-for-http-basic-authentication-in-url-parameters
    public BasicAuth passBaseAuthentication() {
        open("basic_auth", "", "admin", "admin");
        return new BasicAuth();
    }

    public BrokenImages openBrokenImages() {
        brokenImages.click();
        return new BrokenImages();
    }

    public Dropdown openDropdownPage() {
        dropdown.click();
        return new Dropdown();
    }

    public Checkboxes openCheckboxes() {
        checkboxes.click();
        return new Checkboxes();
    }

    public DragAndDrop openDragAndDrop() {
        dragAndDrop.click();
        return  new DragAndDrop();
    }

    public FileUpload openFileUpload() {
        fileUpload.click();
        return new FileUpload();
    }

    public FormAuthentication openFormAuthentication() {
        formAuthentication.click();
        return new FormAuthentication();
    }

    public SortableDataTables openSortableDataTables() {
        sortableDataTables.click();
        return new SortableDataTables();
    }

}
