package example_pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;

import java.util.Arrays;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.junit.jupiter.api.Assertions.assertEquals;

// Два примера на проверку сортировки
// Один для таблицы где отстутсвуют классы и ид, другой где эти идентификаторы присутствуют
// Главное отличие в том что при отсутсвии классов, придётся опираться на индексы
// Для удобства можно в комментарияъ к пейдже расписать соотвествие между индексами и названиями колонок
// Например 1 - last name, 2 - first name и т.д. В дальнейшем при изменении структуры поможет в дебаге
// или можно сделать более универсальный метод получать индекс колонки динмамически
// Сделать можно следующим образом: Собрать все заголовки в массив. Найти в массиве нужное имя и получить его индекс

public class SortableDataTables {
    // handling sorting without ids/classes
    SelenideElement lastNameHeaderWithoutClass = $(By.xpath("//*[text() = 'Last Name']"));
    SelenideElement tableWithoutClasses = $(By.id("table1"));

    // handling table with ids/classed
    ElementsCollection lastNameElements = $$(By.xpath("//td[@class='last-name']"));
    SelenideElement lastNameHeader = $(By.className("last-name"));
    SelenideElement el = $(By.cssSelector("fd"));


    // Methods for without classes

    public SortableDataTables ascSortByLastNameWithClass() {
        lastNameHeaderWithoutClass.click();
        return this;
    }

    public ElementsCollection getColumnElements() {
        ElementsCollection temp = tableWithoutClasses.$$(By.xpath(".//td[position() = 1]"));
        return temp;
    }

    public String[] getLastNameColumnForTableWithoutClasses() {
        String[] lastNames = new String[getColumnElements().size()];
        int i = 0;
        ElementsCollection lastNamesElements = getColumnElements();
        for(SelenideElement lastName: lastNamesElements) {
            lastNames[i++] = lastName.getText();
        }
        return lastNames;
    }

    public void checkSortResultForTableWithoutClasses() {
        String[] actualResult = getLastNameColumnForTableWithoutClasses();
        String[] expectedResult = getLastNameColumnForTableWithoutClasses();
        Arrays.sort(expectedResult);
        Assertions.assertArrayEquals(actualResult, expectedResult, "Sorting by lastName doesn't work correctly");
    }

    // Methods for with classes

    public SortableDataTables ascSortByLastName() {
        lastNameHeader.click();
        return this;
    }

    public String[] getLastNameColumn() {
        String[] lastNames = new String[lastNameElements.size()];
        int i = 0;
        for(SelenideElement lastName: lastNameElements) {
            lastNames[i++] = lastName.getText();
        }
        return lastNames;
    }

    public void checkSortResult() {
        String[] actualResult = getLastNameColumn();
        String[] expectedResult = getLastNameColumn();
        Arrays.sort(expectedResult);
        Assertions.assertArrayEquals(actualResult, expectedResult, "Sorting by lastName doesn't work correctly");
    }



    // soft assert example
    // Используется в случае когда нужно проверить много вещей в одном месте
    // Soft assert нужен для того чтобы все проверки выполнились, так как в случае стандартного hard assert
    // после первой зафейленной проверки выполнение тестов прервётся и результат остальных проверок останется неизвестным

    public void checkFirstRowData() {
        SelenideElement firstRow = tableWithoutClasses.$(By.xpath(".//tr[position() = 2]"));
        ElementsCollection td = firstRow.$$(By.xpath(".//td"));
        Assertions.assertAll("Should return expected data",
                () -> assertEquals("Smith", td.get(0).getText()),
                () -> assertEquals("John", td.get(1).getText()),
                () -> assertEquals("jsmith@gmail.com", td.get(2).getText()),
                () -> assertEquals("$50.00", td.get(3).getText()),
                () -> assertEquals("http://www.jsmith.com", td.get(4).getText())
        );
    }

}
