package example_pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;

public class FileUpload {

    private SelenideElement fileUploader = $(By.id("file-upload"));
    private SelenideElement uploadButton = $(By.id("file-submit"));
    private SelenideElement resultMessage = $(By.tagName("h3"));
    private SelenideElement uploadedFileName = $(By.id("uploaded-files"));

    // Метод для загрузки любого количества файлов, можно передать одно имя, можно передать массив
    public FileUpload uploadFiles(String... fileName) {
        String separator = System.getProperty("file.separator");
        File[] files = new File[fileName.length];
        for(int i = 0; i < fileName.length; i++) {
            files[i] = new File(System.getProperty("user.dir") + separator + "src" + separator + "test" + separator + "resources" +  separator + "test_files" + separator  + fileName[i]);
            fileUploader.uploadFile(files[i]);
        }
        return this;
    }

    public void submit() {
        uploadButton.click();
    }

    public void checkFileIsUploaded(String... fileName) {
        resultMessage.shouldHave(Condition.text("File Uploaded!"));
        for(int i = 0; i < fileName.length; i++) {
            uploadedFileName.shouldHave(Condition.text(fileName[i]));
        }
    }


}
