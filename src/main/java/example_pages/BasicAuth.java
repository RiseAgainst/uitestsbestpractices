package example_pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class BasicAuth {
    private SelenideElement resultMessage = $(By.tagName("p"));
    private String expectedResultMessage =  "Congratulations! You must have the proper credentials.";



    public void checkAuthIsPassed() {
        resultMessage.shouldHave(Condition.text(expectedResultMessage));
    }
}
