package example_pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class DragAndDrop {
    private SelenideElement columnA = $(By.id("column-a"));
    private SelenideElement columnB = $(By.id("column-b"));

    // Drag And Drop метод не работает практически нигде
    // https://automated-testing.info/t/manipulyaczii-so-slajderom-v-selenide-s-pomoshhyu-actions/14750
    // При необходимсоти использования drag and drop это решается стороними тулзами


    public void dragColumnAToColumnB() {
        columnA.dragAndDropTo(columnB);
    }

    public void checkDragAndDropIsSuccessful() {
        columnA.shouldHave(Condition.text("B"));
    }
}
