package example_pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class Dropdown {
    private SelenideElement dropdownElement = $(By.id("dropdown"));


    public void selectValue(String textValue) {
        dropdownElement.selectOption(textValue);

    }

    public void checkSelectedValue(String textValue) {
        dropdownElement.getSelectedOption().shouldHave(Condition.text(textValue));
    }
}
