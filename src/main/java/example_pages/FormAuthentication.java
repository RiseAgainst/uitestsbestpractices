package example_pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import data_models.LoginModel;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class FormAuthentication {
    private SelenideElement username = $(By.id("username"));
    private SelenideElement password = $(By.id("password"));
    private SelenideElement submit = $(By.className("radius"));
    private SelenideElement resultMessage = $(By.id("flash"));


    public FormAuthentication fillLoginForm(LoginModel loginModel) {
        username.sendKeys(loginModel.getUsername());
        password.sendKeys(loginModel.getPassword());
        return this;
    }

    public void submitLogin() {
        submit.click();
    }

    public void checkLoginResult(String expectedMessage) {
        resultMessage.shouldHave(Condition.text(expectedMessage));
    }
}
