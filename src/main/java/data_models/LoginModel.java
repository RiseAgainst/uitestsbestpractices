package data_models;

// Возможно для таких моделей захочется использовать плагин Lombok
// пример я опишу, но реализовывать здесь не буду так как плагин быстро устраревает со сменой версии java/idea и нужно постоянно это поддеживать


public class LoginModel {
    String username;
    String password;

    public LoginModel(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}



//Реализация с Lombok выглядела бы следующим образом

/*
@Data
public class LoginModel {
    String username;
    String password;
}
*/


// https://projectlombok.org/features/all Все возможности плагина
// Более подробное описание работы плагина https://javarush.ru/groups/posts/518-annotacii-chastjh-vtoraja-lombok
// @Data на этапе компиляции сгенерирует геттеры\сеттеры для всех полей, toString и переопределит equals и hashCode по стандартам.
// В IDE можно установить плагин и он будет видеть все ещё не созданные методы.

//To make Lombok work for Intelij Idea:
//Preferences (Ctrl + Alt + S)
//Build, Execution, Deployment
//Compiler
//Annotation Processors
//Enable annotation processing
//Make sure you have the Lombok plugin for IntelliJ installed!
//Preferences -> Plugins
//Search for "Lombok Plugin"
//Click Browse repositories...
//Choose Lombok Plugin
//Install
//Restart IntelliJ